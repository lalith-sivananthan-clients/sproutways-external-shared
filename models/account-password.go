package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBAuthenticationCollectionAccountPasswords *mgo.Collection
	DBAuthenticationSessionAccountPasswords    *mgo.Session
)

type (
	AccountPassword struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Token     string        `bson:"to"             json:"Token"`
		When      WhenToken     `bson:"wh"             json:"When"`
	}

	//--------------------------------------------------------------------------
	// Safe
	//--------------------------------------------------------------------------
)

func init() {
	DBAuthenticationSessionAccountPasswords, DBAuthenticationCollectionAccountPasswords = configs.DBConnection(configs.DBName, configs.DBAuthenticationCollectionAccountPasswordsName)
	defer DBAuthenticationSessionAccountPasswords.Close()
}
