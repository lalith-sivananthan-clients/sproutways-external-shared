package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBDataCollectionShopProducts *mgo.Collection
	DBDataSessionShopProducts    *mgo.Session
)

type (
	ShopProduct struct {
		ID          bson.ObjectId        `bson:"_id,omitempty"  json:"ID"`
		ShopID      bson.ObjectId        `bson:"_sid,omitempty" json:"ShopID"`
		Name        string               `bson:"na"             json:"Name"`
		Slug        string               `bson:"sl"             json:"Slug"`
		Description string               `bson:"de"             json:"Description"`
		SKU         string               `bson:"sk"             json:"SKU"`
		Status      string               `bson:"st"             json:"Status"`
		Category    string               `bson:"ca"             json:"Category"`
		Group       string               `bson:"gr"             json:"Group"`
		Class       string               `bson:"cl"             json:"Class"`
		Method      string               `bson:"me"             json:"Method"`
		Type        string               `bson:"ty"             json:"Type"`
		THC         uint                 `bson:"th"             json:"THC"`
		CBD         uint                 `bson:"cb"             json:"CBD"`
		Marketplace ShopProductInventory `bson:"mp"             json:"Marketplace"`
		Brokerage   ShopProductInventory `bson:"br"             json:"Brokerage"`
		Logo        MediaImageRef        `bson:"lo"             json:"Logo"`
		Cover       MediaImageRef        `bson:"co"             json:"Cover"`
		Gallery     []MediaImageRef      `bson:"ga"             json:"Gallery"`
		When        When                 `bson:"wh"             json:"When"`
	}

	ShopProductInventory struct {
		Status    string `bson:"st" json:"Status"`
		Unit      string `bson:"un" json:"Unit"`
		Price     uint   `bson:"pr" json:"Price"`
		Inventory uint   `bson:"in" json:"Inventory"`
	}

	// simple

	ShopProductSimple struct {
		ID          bson.ObjectId        `bson:"_id,omitempty"  json:"ID"`
		ShopID      bson.ObjectId        `bson:"_sid,omitempty" json:"ShopID"`
		Name        string               `bson:"na"             json:"Name"`
		Slug        string               `bson:"sl"             json:"Slug"`
		SKU         string               `bson:"sk"             json:"SKU"`
		Group       string               `bson:"gr"             json:"Group"`
		Class       string               `bson:"cl"             json:"Class"`
		Method      string               `bson:"me"             json:"Method"`
		Type        string               `bson:"ty"             json:"Type"`
		Marketplace ShopProductInventory `bson:"mp"             json:"Marketplace"`
		Brokerage   ShopProductInventory `bson:"br"             json:"Brokerage"`
		Logo        MediaImageRef        `bson:"lo"             json:"Logo"`
	}

	// explore

	ShopProductSearch struct {
		ID          bson.ObjectId        `bson:"_id,omitempty"  json:"ID"`
		ShopID      bson.ObjectId        `bson:"_sid,omitempty" json:"ShopID"`
		Name        string               `bson:"na"             json:"Name"`
		Slug        string               `bson:"sl"             json:"Slug"`
		Description string               `bson:"de"             json:"Description"`
		SKU         string               `bson:"sk"             json:"SKU"`
		Status      string               `bson:"st"             json:"Status"`
		Category    string               `bson:"ca"             json:"Category"`
		Group       string               `bson:"gr"             json:"Group"`
		Class       string               `bson:"cl"             json:"Class"`
		Method      string               `bson:"me"             json:"Method"`
		Type        string               `bson:"ty"             json:"Type"`
		THC         uint                 `bson:"th"             json:"THC"`
		CBD         uint                 `bson:"cb"             json:"CBD"`
		Marketplace ShopProductInventory `bson:"mp"             json:"Marketplace"`
		Brokerage   ShopProductInventory `bson:"br"             json:"Brokerage"`
		Logo        MediaImageRef        `bson:"lo"             json:"Logo"`
		Cover       MediaImageRef        `bson:"co"             json:"Cover"`
		Gallery     []MediaImageRef      `bson:"ga"             json:"Gallery"`
	}
)

func init() {
	DBDataSessionShopProducts, DBDataCollectionShopProducts = configs.DBConnection(configs.DBName, configs.DBDataCollectionShopProductsName)
	defer DBDataSessionShopProducts.Close()
}
