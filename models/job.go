package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBQueueCollectionJobs *mgo.Collection
	DBQueueSessionJobs    *mgo.Session
)

type (
	Job struct {
		ID       bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		OwnerID  bson.ObjectId `bson:"_oid,omitempty" json:"OwnerID"`
		Type     string        `bson:"ty"             json:"Type"`
		Attempts int           `bson:"at"             json:"Attempts"`
		Data     []interface{} `bson:"da"             json:"Data"`
		When     When          `bson:"wh"             json:"When"`
	}

	JobEmail struct {
		ID       bson.ObjectId `bson:"_id,omitempty" json:"ID"`
		OwnerID  bson.ObjectId `bson:"_oid"          json:"OwnerID"`
		Type     string        `bson:"ty"            json:"Type"`
		Attempts int           `bson:"at"            json:"Attempts"`
		Template string        `bson:"te"            json:"Template"`
		Data     []interface{} `bson:"da"            json:"Data"`
		When     When          `bson:"wh"            json:"When"`
	}
)

func init() {
	DBQueueSessionJobs, DBQueueCollectionJobs = configs.DBConnection(configs.DBName, configs.DBQueueCollectionJobsName)
	defer DBQueueSessionJobs.Close()
}
