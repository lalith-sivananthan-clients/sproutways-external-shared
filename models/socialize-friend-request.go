package models

import (
	"git.sipher.co/sproutways/external/shared/configs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	DBSocializeCollectionFriendRequest *mgo.Collection
	DBSocializeSessionFriendRequest    *mgo.Session
)

type FriendRequestStatus int

const (
	FriendRequestPending FriendRequestStatus = iota
	FriendRequestAccepted
	FriendRequestRejected
)

type FriendRequest struct {
	ID     bson.ObjectId       `bson:"_id,omitempty"   json:"ID"`
	From   bson.ObjectId       `bson:"_from,omitempty" json:"From"`
	To     bson.ObjectId       `bson:"_to"             json:"To"`
	When   When                `bson:"wh"              json:"When"`
	Status FriendRequestStatus `bson:"fs"              json:"Status"`
}

func init() {
	DBSocializeSessionFriendRequest, DBSocializeCollectionFriendRequest = configs.DBConnection(configs.DBName, configs.DBSocializeCollectionFriendRequestName)

	defer DBSocializeSessionFriendRequest.Close()
}
