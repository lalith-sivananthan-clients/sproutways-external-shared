package models

import "time"

type (
	When struct {
		CreatedAt time.Time  `bson:"ca"           json:"CreatedAt"`
		UpdatedAt time.Time  `bson:"ua"           json:"UpdatedAt"`
		DeletedAt *time.Time `bson:"da,omitempty" json:"DeletedAt"`
	}

	WhenToken struct {
		CreatedAt time.Time `bson:"ca" json:"CreatedAt"`
		UpdatedAt time.Time `bson:"ua" json:"UpdatedAt"`
		ExpiresAt time.Time `bson:"ea" json:"ExpiresAt"`
	}
)

func WhenNew() (when When) {

	when = When{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	return
}

func WhenTokenNew() (when WhenToken) {

	when = WhenToken{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		ExpiresAt: time.Now().Add(time.Minute * 60 * 24 * 7),
	}

	return
}
