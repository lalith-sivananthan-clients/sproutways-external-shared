package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBDataCollectionTransactionCarts *mgo.Collection
	DBDataSessionTransactionCarts    *mgo.Session
)

type (
	Cart struct {
		AccountID bson.ObjectId     `bson:"_aid,omitempty" json:"AccountID"`
		ShopID    bson.ObjectId     `bson:"_sid,omitempty" json:"ShopID"`
		ProductID bson.ObjectId     `bson:"_pid"           json:"ProductID"`
		Shop      ShopSimple        `bson:"sh"             json:"Shop"`
		Product   ShopProductSimple `bson:"pr"             json:"Product"`
		Count     uint              `bson:"co"             json:"Count"`
		When      When              `bson:"wh"             json:"When"`
	}

	// simple

	CartSimple struct {
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		ShopID    bson.ObjectId `bson:"_sid,omitempty" json:"ShopID"`
		ProductID bson.ObjectId `bson:"_pid"           json:"ProductID"`
		Count     uint          `bson:"co"             json:"Count"`
		When      When          `bson:"wh"             json:"When"`
	}

	// explore
)

func init() {
	DBDataSessionTransactionCarts, DBDataCollectionTransactionCarts = configs.DBConnection(configs.DBName, configs.DBDataCollectionTransactionCartsName)
	defer DBDataSessionTransactionCarts.Close()
}
