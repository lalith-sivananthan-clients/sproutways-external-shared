package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBAuthenticationCollectionAccountEmails *mgo.Collection
	DBAuthenticationSessionAccountEmails    *mgo.Session
)

type (
	AccountEmail struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Token     string        `bson:"to"             json:"Token"`
		Email     string        `bson:"em"             json:"Email"`
		When      WhenToken     `bson:"wh"             json:"When"`
	}

	//--------------------------------------------------------------------------
	// Safe
	//--------------------------------------------------------------------------
)

func init() {
	DBAuthenticationSessionAccountEmails, DBAuthenticationCollectionAccountEmails = configs.DBConnection(configs.DBName, configs.DBAuthenticationCollectionAccountEmailsName)
	defer DBAuthenticationSessionAccountEmails.Close()
}
