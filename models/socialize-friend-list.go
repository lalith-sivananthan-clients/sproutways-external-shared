package models

import (
	"git.sipher.co/sproutways/external/shared/configs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	DBSocializeCollectionFriendList *mgo.Collection
	DBSocializeSessionFriendList    *mgo.Session
)

type FriendList struct {
	ID      bson.ObjectId   `bson:"_id,omitempty" json:"ID"`
	Friends []bson.ObjectId `bson:"fs" json:"Friends"`
}

func init() {
	DBSocializeSessionFriendList, DBSocializeCollectionFriendList = configs.DBConnection(configs.DBName, configs.DBSocializeCollectionFriendListName)

	defer DBSocializeSessionFriendList.Close()
}
