package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBStorageCollectionMedia *mgo.Collection
	DBStorageSessionMedia    *mgo.Session
)

type (
	Media struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Status    string        `bson:"st"             json:"Status"`
		Type      string        `bson:"ty"             json:"Type"`
		Location  string        `bson:"lo"             json:"Location"`
		Mime      string        `bson:"mi"             json:"Mime"`
		When      When          `bson:"wh"             json:"When"`
	}

	MediaImage struct {
		ID        bson.ObjectId     `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId     `bson:"_aid,omitempty" json:"AccountID"`
		Status    string            `bson:"st"             json:"Status"`
		Type      string            `bson:"ty"             json:"Type"`
		Location  string            `bson:"lo"             json:"Location"`
		Mime      string            `bson:"mi"             json:"Mime"`
		Crop      MediaImageCrop    `bson:"cr"             json:"Crop"`
		For       string            `bson:"fo"             json:"For"`
		ForSizes  []MediaImageSizes `bson:"fs"             json:"ForSizes"`
		When      When              `bson:"wh"             json:"When"`
	}

	MediaImageRef struct {
		MediaID bson.ObjectId `bson:"_mid,omitempty" json:"MediaID"`
		Status  string        `bson:"st"             json:"Status"`
	}

	MediaImageCrop struct {
		XOffset int  `bson:"xo" json:"XOffset"`
		YOffset int  `bson:"yo" json:"YOffset"`
		Width   uint `bson:"wi" json:"Width"`
		Height  uint `bson:"he" json:"Height"`
	}

	MediaImageSizes struct {
		Size  string `bson:"si" json:"Size"`
		Exist bool   `bson:"ex" json:"Exist"`
	}

	//--------------------------------------------------------------------------
	// Safe
	//--------------------------------------------------------------------------

)

func init() {
	DBStorageSessionMedia, DBStorageCollectionMedia = configs.DBConnection(configs.DBName, configs.DBStorageCollectionMediaName)
	defer DBStorageSessionMedia.Close()
}
