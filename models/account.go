package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBAuthenticationCollectionAccounts *mgo.Collection
	DBAuthenticationSessionAccounts    *mgo.Session
)

type (
	Account struct {
		ID           bson.ObjectId       `bson:"_id,omitempty" json:"ID"`
		Email        string              `bson:"em"            json:"Email"`
		Username     string              `bson:"un"            json:"Username"`
		Password     string              `bson:"pw"            json:"Password"`
		PasswordSet  bool                `bson:"ps"            json:"PasswordSet"`
		Confirmed    bool                `bson:"co"            json:"Confirmed"`
		Status       string              `bson:"st"            json:"Status"`
		Profile      AccountProfile      `bson:"pr"            json:"Profile"`
		Subscription AccountSubscription `bson:"su"            json:"Subscription"`
		Connections  AccountConnections  `bson:"cn"            json:"Connections"`
		When         When                `bson:"wh"            json:"When"`
	}

	AccountProfile struct {
		Avatar    MediaImageRef `bson:"av" json:"Avatar"`
		NameFirst string        `bson:"nf" json:"NameFirst"`
		NameLast  string        `bson:"nl" json:"NameLast"`
		Headline  string        `bson:"hl" json:"Headline"`
		Story     string        `bson:"st" json:"Story"`
	}

	AccountSubscription struct {
		MarketplaceLevel  string     `bson:"ml" json:"MarketplaceLevel"`
		MarketplaceExpire *time.Time `bson:"me" json:"MarketplaceExpire"`
		BrokerageLevel    string     `bson:"bl" json:"BrokerageLevel"`
		BrokerageExpire   *time.Time `bson:"be" json:"BrokerageExpire"`
	}

	AccountConnections struct {
		Facebook AccountConnectionsFacebook `bson:"fb" json:"Facebook"`
		Google   AccountConnectionsGoogle   `bson:"go" json:"Google"`
		LinkedIn AccountConnectionsLinkedIn `bson:"li" json:"LinkedIn"`
	}

	AccountConnectionsFacebook struct {
		Connected    bool      `bson:"co"           json:"Connected"`
		ID           string    `bson:"id"           json:"ID"`
		Token        string    `bson:"to"           json:"Token"`
		TokenType    string    `bson:"tt"           json:"TokenType"`
		TokenRefresh string    `bson:"tr"           json:"TokenRefresh"`
		TokenExpires time.Time `bson:"te,omitempty" json:"TokenExpires"`
		Email        string    `bson:"em"           json:"Email"`
	}

	AccountConnectionsGoogle struct {
		Connected    bool      `bson:"co"           json:"Connected"`
		ID           string    `bson:"id"           json:"ID"`
		Token        string    `bson:"to"           json:"Token"`
		TokenType    string    `bson:"tt"           json:"TokenType"`
		TokenRefresh string    `bson:"tr"           json:"TokenRefresh"`
		TokenExpires time.Time `bson:"te,omitempty" json:"TokenExpires"`
		Email        string    `bson:"em"           json:"Email"`
	}

	AccountConnectionsLinkedIn struct {
		Connected    bool      `bson:"co"           json:"Connected"`
		ID           string    `bson:"id"           json:"ID"`
		Token        string    `bson:"to"           json:"Token"`
		TokenType    string    `bson:"tt"           json:"TokenType"`
		TokenRefresh string    `bson:"tr"           json:"TokenRefresh"`
		TokenExpires time.Time `bson:"te,omitempty" json:"TokenExpires"`
		Email        string    `bson:"em"           json:"Email"`
	}

	// explore

	AccountSearch struct {
		ID       bson.ObjectId  `bson:"_id,omitempty" json:"ID"`
		Username string         `bson:"un"            json:"Username"`
		Profile  AccountProfile `bson:"pr"            json:"Profile"`
	}
)

func init() {
	DBAuthenticationSessionAccounts, DBAuthenticationCollectionAccounts = configs.DBConnection(configs.DBName, configs.DBAuthenticationCollectionAccountsName)
	defer DBAuthenticationSessionAccounts.Close()
}
