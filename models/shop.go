package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBDataCollectionShops *mgo.Collection
	DBDataSessionShops    *mgo.Session
)

type (
	Shop struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Status    string        `bson:"st"             json:"Status"`
		Name      string        `bson:"na"             json:"Name"`
		Slug      string        `bson:"sl"             json:"Slug"`
		Story     string        `bson:"sy"             json:"Story"`
		Logo      MediaImageRef `bson:"lg"             json:"Logo"`
		Cover     MediaImageRef `bson:"co"             json:"Cover"`
		Links     ShopLinks     `bson:"li"             json:"Links"`
		Tutorial  ShopTutorial  `bson:"tu"             json:"Tutorial"`
		Settings  ShopSettings  `bson:"se"             json:"Settings"`
		When      When          `bson:"wh"             json:"When"`
	}

	ShopLinks struct {
		Website   string `bson:"ws" json:"Website"`
		Blog      string `bson:"bl" json:"Blog"`
		Facebook  string `bson:"fb" json:"Facebook"`
		Instagram string `bson:"ig" json:"Instagram"`
		Twitter   string `bson:"tw" json:"Twitter"`
		LinkedIn  string `bson:"li" json:"LinkedIn"`
	}

	ShopTutorial struct {
		Step0 bool `bson:"s1" json:"Step0"`
		Step1 bool `bson:"s2" json:"Step1"`
		Step2 bool `bson:"s3" json:"Step2"`
		Step3 bool `bson:"s4" json:"Step3"`
		Step5 bool `bson:"s5" json:"Step4"`
	}

	ShopSettings struct {
		PrimaryContact  ShopSettingsPrimaryContact  `bson:"pc" json:"PrimaryContact"`
		BusinessDetail  ShopSettingsBusinessDetail  `bson:"bd" json:"BusinessDetail"`
		BusinessLicense ShopSettingsBusinessLicense `bson:"bl" json:"BusinessLicense"`
		BusinessContact ShopSettingsBusinessContact `bson:"bc" json:"BusinessContact"`
	}

	ShopSettingsPrimaryContact struct {
		NameFirst string        `bson:"nf" json:"NameFirst"`
		NameLast  string        `bson:"nl" json:"NameLast"`
		Day       string        `bson:"da" json:"Day"`
		Month     string        `bson:"mo" json:"Month"`
		Year      string        `bson:"ye" json:"Year"`
		Phone     string        `bson:"ph" json:"Phone"`
		Email     string        `bson:"em" json:"Email"`
		IDFront   MediaImageRef `bson:"if" json:"IDFront"`
		IDBack    MediaImageRef `bson:"ib" json:"IDBack"`
	}

	ShopSettingsBusinessDetail struct {
		Name string `bson:"na" json:"Name"`
		Type string `bson:"ty" json:"Type"`
		ID   string `bson:"id" json:"ID"`
	}

	ShopSettingsBusinessLicense struct {
		Number      string `bson:"nu" json:"Number"`
		Designation string `bson:"de" json:"Designation"`
		Type        string `bson:"ty" json:"Type"`
	}

	ShopSettingsBusinessContact struct {
		Address1 string `bson:"a1" json:"Address1"`
		Address2 string `bson:"a2" json:"Address2"`
		City     string `bson:"ci" json:"City"`
		State    string `bson:"st" json:"State"`
		Country  string `bson:"co" json:"Country"`
		Zip      string `bson:"zi" json:"Zip"`
		Phone    string `bson:"ph" json:"Phone"`
		Email    string `bson:"em" json:"Email"`
	}

	// simple

	ShopSimple struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Name      string        `bson:"na"             json:"Name"`
		Slug      string        `bson:"sl"             json:"Slug"`
		Logo      MediaImageRef `bson:"lg"             json:"Logo"`
	}

	// explore

	ShopSearch struct {
		ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
		Status    string        `bson:"st"             json:"Status"`
		Name      string        `bson:"na"             json:"Name"`
		Slug      string        `bson:"sl"             json:"Slug"`
		Story     string        `bson:"sy"             json:"Story"`
		Logo      MediaImageRef `bson:"lg"             json:"Logo"`
		Cover     MediaImageRef `bson:"co"             json:"Cover"`
		Links     ShopLinks     `bson:"li"             json:"Links"`
	}
)

func init() {
	DBDataSessionShops, DBDataCollectionShops = configs.DBConnection(configs.DBName, configs.DBDataCollectionShopsName)
	defer DBDataSessionShops.Close()
}
