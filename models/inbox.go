package models

import (
	"time"

	"git.sipher.co/sproutways/external/shared/configs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	DBInboxCollectionConversation *mgo.Collection
	DBInboxSessionConversation    *mgo.Session
)

type Conversation struct {
	ID           bson.ObjectId   `bson:"_id,omitempty" json:"ID"`
	Participants []bson.ObjectId `bson:"pt"            json:"Participants"`
	Messages     []Message       `bson:"ms"            json:"Messages"`
}

type Message struct {
	From      bson.ObjectId `bson:"_fid,omitempty" json:"From"`
	To        bson.ObjectId `bson:"_tid,omitempty" json:"To"`
	Msg       string        `bson:"ms"             json:"Msg"`
	Type      string        `bson:"ty"             json:"Type"`
	CreatedAt time.Time     `bson:"ca"             json:"CreatedAt"`
}

func init() {
	DBInboxSessionConversation, DBInboxCollectionConversation = configs.DBConnection(configs.DBName, configs.DBInboxCollectionConversation)

	defer DBInboxSessionConversation.Close()
}
