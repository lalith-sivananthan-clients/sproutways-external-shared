package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
)

var (
	DBDataCollectionTransactionOrders *mgo.Collection
	DBDataSessionTransactionOrders    *mgo.Session
)

type (
	Order struct {
		ID        bson.ObjectId  `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId  `bson:"_aid,omitempty" json:"AccountID"`
		ShopID    bson.ObjectId  `bson:"_sid,omitempty" json:"ShopID"`
		Status    string         `bson:"st"             json:"Status"`
		Shop      ShopSimple     `bson:"sh"             json:"Shop"`
		Products  []OrderProduct `bson:"pr"             json:"Products"`
		When      When           `bson:"wh"             json:"When"`
	}

	OrderProduct struct {
		ProductID bson.ObjectId     `bson:"_pid,omitempty" json:"ProductID"`
		Product   ShopProductSimple `bson:"pr"             json:"Product"`
		Quanitity uint              `bson:"qu"             json:"Quanitity"`
	}

	// simple

	OrderSimple struct {
		ID        bson.ObjectId        `bson:"_id,omitempty"  json:"ID"`
		AccountID bson.ObjectId        `bson:"_aid,omitempty" json:"AccountID"`
		ShopID    bson.ObjectId        `bson:"_sid,omitempty" json:"ShopID"`
		Status    string               `bson:"st"             json:"Status"`
		Products  []OrderProductSimple `bson:"pr"             json:"Products"`
		When      When                 `bson:"wh"             json:"When"`
	}

	OrderProductSimple struct {
		ProductID bson.ObjectId `bson:"_pid,omitempty" json:"ProductID"`
		Quanitity uint          `bson:"qu"             json:"Quanitity"`
	}

	// explore
)

func init() {
	DBDataSessionTransactionOrders, DBDataCollectionTransactionOrders = configs.DBConnection(configs.DBName, configs.DBDataCollectionTransactionOrdersName)
	defer DBDataSessionTransactionOrders.Close()
}
