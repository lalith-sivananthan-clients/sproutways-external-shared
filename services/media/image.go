package media

import (
	"errors"
	"image/jpeg"
	"io"
	"net/http"
	"os"
	"strconv"

	"gopkg.in/gographics/imagick.v3/imagick"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/models"
)

func init() {
}

//var imageData models.Media
//var imageUploaded *os.File
//var magickWand *imagick.MagickWand

func Flatten(fromPath string, fromFileName string, toPath string, toFileName string) (err error) {

	// imagick : init
	imagick.Initialize()
	defer imagick.Terminate()

	// imagick : init -> wand
	wand := imagick.NewMagickWand()
	defer wand.Destroy()

	//// set : resolution
	//err = wand.SetResolution(72, 72)
	//if err != nil {
	//	return
	//}

	// image : open
	err = wand.ReadImage(fromPath + fromFileName)
	if err != nil {
		return
	}

	// image : flatten
	pixelWand := imagick.NewPixelWand()
	pixelWand.SetColor("white")

	err = wand.SetImageBackgroundColor(pixelWand)
	if err != nil {
		return
	}

	wand.MergeImageLayers(imagick.IMAGE_LAYER_FLATTEN)

	// image : quality
	err = wand.SetCompressionQuality(100)
	if err != nil {
		return
	}

	// image : convert
	err = wand.SetFormat("jpg")
	if err != nil {
		return
	}

	err = wand.WriteImage(toPath + toFileName + "_original.jpg")

	return
}

func CopyPaste(fromPath string, fromFileName string, toPath string, toFileName string) (err error) {

	// file : source -> open
	from, err := os.Open(fromPath + fromFileName)
	if err != nil {

		return
	}
	defer from.Close()

	// file : source -> check
	fromStats, err := from.Stat()
	if err != nil {
		return
	}

	if !fromStats.Mode().IsRegular() {
		err = errors.New("not a file")
		return
	}

	// file : source -> get : content type
	contentType, err := GetFileContentType(from)
	if err != nil {
		return
	}

	// file : source -> validate & process
	if contentType == "image/jpeg" {
		_, err := jpeg.Decode(from)
		if err == nil {
			// file : destination -> create
			to, err := os.Create(toPath + toFileName + "_original.jpg")
			if err != nil {
				return err
			}
			defer to.Close()

			// file : copy
			_, err = io.Copy(to, from)
			if err != nil {
				return err
			}

			// file : sync
			err = to.Sync()
			if err != nil {
				return err
			}
		}

		err = Flatten(fromPath, fromFileName, toPath, toFileName)
		if err != nil {
			return err
		}
	} else if contentType == "image/png" {
		err = Flatten(fromPath, fromFileName, toPath, toFileName)
	} else {
		err = errors.New("invalid image type")
	}

	return
}

func GetFileContentType(out *os.File) (contentType string, err error) {
	buffer := make([]byte, 512)
	_, err = out.Read(buffer)
	if err != nil {
		return "", err
	}

	contentType = http.DetectContentType(buffer)

	return
}

func Crop2(fromPath string, fromFileName string, xOffset int, yOffset int, width uint, height uint) (err error) {

	// imagick : init
	imagick.Initialize()
	defer imagick.Terminate()

	// imagick : init -> wand
	wand := imagick.NewMagickWand()
	defer wand.Destroy()

	//// set : resolution
	//err = wand.SetResolution(72, 72)
	//if err != nil {
	//	spew.Dump("1")
	//	return
	//}

	// image : open
	err = wand.ReadImage(fromPath + fromFileName + "_original.jpg")
	if err != nil {
		return
	}

	if wand.GetImageWidth() > wand.GetImageHeight() {
		// image : landscape
		newWidth := (height * wand.GetImageWidth()) / wand.GetImageHeight()
		newHeight := height

		err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}

		err = wand.CropImage(width, height, xOffset, yOffset)
		if err != nil {
			return
		}
	} else if wand.GetImageHeight() > wand.GetImageWidth() {
		// image : portrait
		newWidth := width
		newHeight := (width * wand.GetImageHeight()) / wand.GetImageWidth()

		err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}

		err = wand.CropImage(width, height, xOffset, yOffset)
		if err != nil {
			return
		}
	} else {
		// image : square
		err = wand.ResizeImage(width, height, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}
	}

	err = wand.SetImageCompressionQuality(100)
	if err != nil {
		return
	}

	err = wand.WriteImage(fromPath + fromFileName + "_" + strconv.Itoa(int(width)) + "_" + strconv.Itoa(int(height)) + ".jpg")

	return
}

func Crop(fromPath string, fromFileName string, xOffset int, yOffset int, width uint, height uint) (err error) {

	// imagick : init
	imagick.Initialize()
	defer imagick.Terminate()

	// imagick : init -> wand
	wand := imagick.NewMagickWand()
	defer wand.Destroy()

	//// set : resolution
	//err = wand.SetResolution(72, 72)
	//if err != nil {
	//	spew.Dump("1")
	//	return
	//}

	// image : open
	err = wand.ReadImage(fromPath + fromFileName + "_original.jpg")
	if err != nil {
		return
	}

	err = wand.CropImage(width, height, xOffset, yOffset)
	if err != nil {
		return
	}

	err = wand.SetImageCompressionQuality(100)
	if err != nil {
		return
	}

	err = wand.WriteImage(fromPath + fromFileName + "_cropped.jpg")

	//if wand.GetImageWidth() > wand.GetImageHeight() {
	//	// image : landscape
	//	newWidth := (height * wand.GetImageWidth()) / wand.GetImageHeight()
	//	newHeight := height
	//
	//	err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
	//	if err != nil {
	//		return
	//	}
	//
	//	err = wand.CropImage(width, height, xOffset, yOffset)
	//	if err != nil {
	//		return
	//	}
	//} else if wand.GetImageHeight() > wand.GetImageWidth() {
	//	// image : portrait
	//	newWidth := width
	//	newHeight := (width * wand.GetImageHeight()) / wand.GetImageWidth()
	//
	//	err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
	//	if err != nil {
	//		return
	//	}
	//
	//	err = wand.CropImage(width, height, xOffset, yOffset)
	//	if err != nil {
	//		return
	//	}
	//} else {
	//	// image : square
	//	err = wand.ResizeImage(width, height, imagick.FILTER_LANCZOS)
	//	if err != nil {
	//		return
	//	}
	//}
	//
	//err = wand.SetImageCompressionQuality(100)
	//if err != nil {
	//	return
	//}
	//
	//err = wand.WriteImage(fromPath + fromFileName + "_" + strconv.Itoa(int(width)) + "_" + strconv.Itoa(int(height)) + ".jpg")

	return
}

func Resize(fromPath string, fromFileName string, width uint, height uint) (err error) {

	// imagick : init
	imagick.Initialize()
	defer imagick.Terminate()

	// imagick : init -> wand
	wand := imagick.NewMagickWand()
	defer wand.Destroy()

	//// set : resolution
	//err = wand.SetResolution(72, 72)
	//if err != nil {
	//	spew.Dump("1")
	//	return
	//}

	// image : open
	err = wand.ReadImage(fromPath + fromFileName + "_cropped.jpg")
	if err != nil {
		return
	}

	if wand.GetImageWidth() > wand.GetImageHeight() {
		// image : landscape
		newWidth := (height * wand.GetImageWidth()) / wand.GetImageHeight()
		newHeight := height

		err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}
	} else if wand.GetImageHeight() > wand.GetImageWidth() {
		// image : portrait
		newWidth := width
		newHeight := (width * wand.GetImageHeight()) / wand.GetImageWidth()

		err = wand.ResizeImage(newWidth, newHeight, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}
	} else {
		// image : square
		err = wand.ResizeImage(width, height, imagick.FILTER_LANCZOS)
		if err != nil {
			return
		}
	}

	err = wand.SetImageCompressionQuality(100)
	if err != nil {
		return
	}

	err = wand.WriteImage(fromPath + fromFileName + "_" + strconv.Itoa(int(width)) + "_" + strconv.Itoa(int(height)) + ".jpg")

	return
}

// file -> save original
func SaveOriginal(source *os.File, media models.Media) (err error) {

	// file : source -> get : content type
	contentType, err := GetFileContentType(source)
	if err != nil {
		return
	}

	var path string

	// file : source -> validate & process
	if contentType == "image/jpeg" {
		path = configs.PathGatewayMedia + media.Location + media.ID.Hex() + "_temp.jpg"
	} else if contentType == "image/png" {
		path = configs.PathGatewayMedia + media.Location + media.ID.Hex() + "_temp.png"
	} else {
		err = errors.New("invalid image type")
		return err
	}

	// file : destination -> create
	destination, err := os.Create(path)
	if err != nil {
		return err
	}
	defer destination.Close()

	// file : copy
	_, err = io.Copy(destination, source)
	if err != nil {
		return err
	}

	// file : sync
	err = destination.Sync()

	return
}

//// Crop -> Save changed
//func ImageCrop(XOffset int, YOffset int) (err error, code float32) {
//	// image -> get initial dimensions
//	width := magickWand.GetImageWidth()
//	height := magickWand.GetImageHeight()
//
//	// image -> crop
//	err = magickWand.CropImage(width, height, XOffset, YOffset)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	newWidth := int(width) - XOffset
//	newHeight := int(height) - YOffset
//
//	updatedImageName := "/store/media/media/" + imageData.AccountID.Hex() + "/" + imageData.ID.Hex() + "_cropped_" + strconv.Itoa(newWidth) + "_" + strconv.Itoa(newHeight) + ".jpeg"
//	imageCropped, err := os.Create(updatedImageName)
//
//	if err != nil {
//		code = codes.ErrorModelStorageMediaUpdate
//		return
//	}
//
//	// file -> save
//	err = magickWand.WriteImageFile(imageCropped)
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// image -> display
//	err = magickWand.DisplayImage(os.Getenv("DISPLAY"))
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	return err, code
//}
//
//// Resize -> Save resized
//func ImageResizeFixed() (err error, code float32) {
//	// image -> resize 100 x 100
//	err = magickWand.ResizeImage(100, 100, imagick.FILTER_LANCZOS, 1)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// set quality -> 95%
//	err = magickWand.SetImageCompressionQuality(95)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	updatedImageName := "/store/media/media/" + imageData.AccountID.Hex() + "/" + imageData.ID.Hex() + "_100_100.jpeg"
//
//	file100x100, err := os.Create(updatedImageName)
//
//	if err != nil {
//		code = codes.ErrorModelStorageMediaUpdate
//		return
//	}
//
//	// image -> resize 250 x 250
//	err = magickWand.ResizeImage(100, 100, imagick.FILTER_LANCZOS, 1)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// set quality -> 95%
//	err = magickWand.SetImageCompressionQuality(95)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// file -> save
//	err = magickWand.WriteImageFile(file100x100)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	updatedImageName = "/store/media/media/" + imageData.AccountID.Hex() + "/" + imageData.ID.Hex() + "_250_250.jpeg"
//
//	file250x250, err := os.Create(updatedImageName)
//
//	if err != nil {
//		code = codes.ErrorModelStorageMediaUpdate
//		return
//	}
//
//	// image -> resize 500 x 500
//	err = magickWand.ResizeImage(500, 500, imagick.FILTER_LANCZOS, 1)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// set quality -> 95%
//	err = magickWand.SetImageCompressionQuality(95)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// file -> save
//	err = magickWand.WriteImageFile(file250x250)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	updatedImageName = "/store/media/media/" + imageData.AccountID.Hex() + "/" + imageData.ID.Hex() + "_500_500.jpeg"
//
//	file500x500, err := os.Create(updatedImageName)
//
//	if err != nil {
//		code = codes.ErrorModelStorageMediaUpdate
//		return
//	}
//
//	// file -> save
//	err = magickWand.WriteImageFile(file500x500)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// image -> display
//	err = magickWand.DisplayImage(os.Getenv("DISPLAY"))
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	return err, code
//}
//
//func ImageResizeCustom(width uint, height uint) (err error, code float32) {
//	// image -> resize
//	err = magickWand.ResizeImage(width, height, imagick.FILTER_LANCZOS, 1)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// set quality -> 95%
//	err = magickWand.SetImageCompressionQuality(95)
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	newWidth := strconv.Itoa(int(width))
//	newHeight := strconv.Itoa(int(height))
//
//	updatedImageName := "/store/media/media/" + imageData.AccountID.Hex() + "/" + imageData.ID.Hex() + "_" + newWidth + "_" + newHeight + ".jpeg"
//
//	imageCustom, err := os.Create(updatedImageName)
//
//	if err != nil {
//		code = codes.ErrorModelStorageMediaUpdate
//		return
//	}
//
//	// file -> save
//	err = magickWand.WriteImageFile(imageCustom)
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	// image -> display
//	err = magickWand.DisplayImage(os.Getenv("DISPLAY"))
//
//	if err != nil {
//		code = codes.ErrorUnknown
//		return
//	}
//
//	return err, code
//}
//
//func ImageClose() bool {
//	magickWand.Clear()
//
//	return true
//}
