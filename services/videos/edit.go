package videos

import (
	"io"
	"os"

	"gopkg.in/mgo.v2/bson"
	"github.com/xfrr/goffmpeg/transcoder"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/codes"
)

var videoData models.Media
var videoUploaded *os.File

func SaveOriginal(temporaryVideo *os.File, contentType string, fileExtension string, accountId bson.ObjectId) (err error, code float32, fileId string) {
	// db -> media -> refresh
	models.DBStorageSessionMedia.Refresh()

	// db : video -> data
	videoData = models.Media{
		ID:          bson.NewObjectId(),
		AccountID:   accountId,
		ContentType: contentType,
		Location:    "",
		When:        models.When{},
	}

	videoData.Location = "/store/media/videos/" + accountId.Hex() + "/" + videoData.ID.Hex() + "_original." + fileExtension

	// path : create
	_, err = os.Stat(videoData.Location)

	if os.IsExist(err) {
		videoUploaded, err = os.Create(videoData.Location)
		defer videoUploaded.Close()

		if err != nil {
			code = codes.ErrorUnknown
			return
		}
	}

	// db : media -> create
	err = models.DBStorageCollectionMedia.Insert(&videoData)

	if err != nil {
		code = codes.ErrorModelStorageMediaCreate
		return
	}

	_, err = io.Copy(videoUploaded, temporaryVideo)

	if err != nil {
		code = codes.ErrorModelStorageMediaCreate
		return
	}

	return err, code, videoData.ID.Hex()
}

func VideoFormat() (err error, code float32) {
	// video -> format
	videoTranscoder := new(transcoder.Transcoder)

	inputPath := videoData.Location

	// video -> resize 1080p
	outputPath1080p := "/store/media/videos/" + videoData.AccountID.Hex() + "/" + videoData.ID.Hex() + "_1080p.avi"

	err = videoTranscoder.Initialize(inputPath, outputPath1080p)

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> set resolution -> 1920 x 1080
	videoTranscoder.MediaFile().SetResolution("1920x1080")
	videoTranscoder.MediaFile().SetVideoCodec("standard")

	transcodingDone := videoTranscoder.Run(false)
	err = <-transcodingDone

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> resize 720p
	outputPath720p := "/store/media/videos/" + videoData.AccountID.Hex() + "/" + videoData.ID.Hex() + "_720p.avi"

	err = videoTranscoder.Initialize(inputPath, outputPath720p)

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> set resolution -> 1280 x 720
	videoTranscoder.MediaFile().SetResolution("1280x720") // check
	videoTranscoder.MediaFile().SetVideoCodec("standard")

	transcodingDone = videoTranscoder.Run(false)
	err = <-transcodingDone

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> resize 480p
	outputPath480p := "/store/media/videos/" + videoData.AccountID.Hex() + "/" + videoData.ID.Hex() + "_480p.avi"

	err = videoTranscoder.Initialize(inputPath, outputPath480p)

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> set resolution -> 854 x 480
	videoTranscoder.MediaFile().SetResolution("854×480")
	videoTranscoder.MediaFile().SetVideoCodec("standard")

	transcodingDone = videoTranscoder.Run(false)
	err = <-transcodingDone

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> resize 360p
	outputPath360p := "/store/media/videos/" + videoData.AccountID.Hex() + "/" + videoData.ID.Hex() + "_360p.avi"

	err = videoTranscoder.Initialize(inputPath, outputPath360p)

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	// video -> set resolution -> 640 x 360
	videoTranscoder.MediaFile().SetResolution("640×360")
	videoTranscoder.MediaFile().SetVideoCodec("standard")

	transcodingDone = videoTranscoder.Run(false)
	err = <-transcodingDone

	if err != nil {
		code = codes.ErrorUnknown
		return
	}

	return err, code
}

func CheckVideoContentType(contentType string) bool {
	allowedContentTypes := [...]string{"video/x-flv", "video/mp4", "application/x-mpegURL", "video/MP2T",
		"video/3gpp", "video/quicktime", "video/x-msvideo", "video/x-ms-wmv"}

	for _, a := range allowedContentTypes {
		if a == contentType {
			return true
		}
	}

	return false
}
