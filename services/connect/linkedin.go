package connect

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"git.sipher.co/sproutways/external/shared/configs"
)

type (
	DataLinkedInToken struct {
		AccessToken string `json:"access_token"`
		ExpiresIn   int32  `json:"expires_in"`
	}

	DataLinkedInUser struct {
		ID        string `json:"id"`
		NameFirst string `json:"firstName"`
		NameLast  string `json:"lastName"`
		Email     string `json:"emailAddress"`
	}
)

func GetLinkedInToken(code string) (token DataLinkedInToken, err error) {

	uri := configs.GetProvider().LinkedIn.EndpointOAuth + "accessToken"

	data := url.Values{}
	data.Add("code", code)
	data.Add("client_id", configs.GetProvider().LinkedIn.ClientID)
	data.Add("client_secret", configs.GetProvider().LinkedIn.Secret)
	data.Add("redirect_uri", configs.GetProvider().LinkedIn.Callback)
	data.Add("grant_type", "authorization_code")

	request, err := http.NewRequest("POST", uri, bytes.NewBufferString(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &token)

	return
}

func GetLinkedInUser(token string) (user DataLinkedInUser, err error) {

	uri := configs.GetProvider().LinkedIn.EndpointAPI
	uri = uri + "people/~:(id,email-address)?format=json"

	request, err := http.NewRequest("GET", uri, nil)
	request.Header.Add("Authorization", "Bearer "+token)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &user)

	return
}

func RevokeLinkedInAccess(token string) (err error) {
	if len(token) == 0 {
		err = errors.New("token missing")
		return
	}

	uri := "https://api.linkedin.com/uas/oauth/invalidateToken?access_token=" + token

	request, err := http.NewRequest("GET", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	_, err = ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	return
}
