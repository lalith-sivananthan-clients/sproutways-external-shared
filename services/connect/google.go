package connect

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"git.sipher.co/sproutways/external/shared/configs"
)

type (
	DataGoogleToken struct {
		AccessToken  string `json:"access_token"`
		RefreshToken string `json:"refresh_token"`
		TokenType    string `json:"token_type"`
		ExpiresIn    int32  `json:"expires_in"`
	}

	DataGoogleUser struct {
		ID    string `json:"id"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}
)

func GetGoogleToken(code string) (token DataGoogleToken, err error) {
	uri := configs.GetProvider().Google.EndpointOAuth + "v4/token"

	data := url.Values{}
	data.Add("code", code)
	data.Add("client_id", configs.GetProvider().Google.ClientID)
	data.Add("client_secret", configs.GetProvider().Google.Secret)
	data.Add("redirect_uri", configs.GetProvider().Google.Callback)
	data.Add("grant_type", "authorization_code")

	request, err := http.NewRequest("POST", uri, bytes.NewBufferString(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &token)

	return
}

func GetGoogleUser(token string) (user DataGoogleUser, err error) {
	uri := configs.GetProvider().Google.EndpointOAuth
	uri = uri + "v1/userinfo?"
	uri = uri + "access_token=" + token

	request, err := http.NewRequest("GET", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &user)

	return
}

func RevokeGoogleAccess(token string) (err error) {
	if len(token) == 0 {
		err = errors.New("token missing")
		return
	}

	uri := "https://accounts.google.com/o/oauth2/revoke?token=" + token

	request, err := http.NewRequest("GET", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	_, err = ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	return
}
