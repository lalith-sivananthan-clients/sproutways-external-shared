package connect

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"git.sipher.co/sproutways/external/shared/configs"
)

type (
	DataFacebookToken struct {
		AccessToken string `json:"access_token"`
		TokenType   string `json:"token_type"`
		ExpiresIn   int32  `json:"expires_in"`
	}

	DataFacebookUser struct {
		ID    string `json:"id"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}
)

func GetFacebookToken(code string) (token DataFacebookToken, err error) {
	uri := configs.GetProvider().Facebook.EndpointOAuth
	uri = uri + "oauth/access_token?"
	uri = uri + "&client_id=" + configs.GetProvider().Facebook.ClientID
	uri = uri + "&client_secret=" + configs.GetProvider().Facebook.Secret
	uri = uri + "&redirect_uri=" + configs.GetProvider().Facebook.Callback
	uri = uri + "&code=" + code

	request, err := http.NewRequest("GET", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &token)

	return
}

func GetFacebookUser(token string) (user DataFacebookUser, err error) {
	uri := configs.GetProvider().Facebook.EndpointOAuth
	uri = uri + "me?fields=id,name,email"
	uri = uri + "&client_id=" + configs.GetProvider().Facebook.ClientID
	uri = uri + "&client_secret=" + configs.GetProvider().Facebook.Secret
	uri = uri + "&access_token=" + token

	request, err := http.NewRequest("GET", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	err = json.Unmarshal(body, &user)

	return
}

func RevokeFacebookAccess(token string) (err error) {
	if len(token) == 0 {
		err = errors.New("token missing")
		return
	}

	uri := configs.GetProvider().Facebook.EndpointOAuth
	uri = uri + "me/permissions"
	uri = uri + "?access_token=" + token

	request, err := http.NewRequest("DELETE", uri, nil)

	if err != nil {
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return
	}

	defer response.Body.Close()

	_, err = ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	return
}
