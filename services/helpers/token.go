package helpers

import (
	"log"
	"math/rand"
	"time"

	"github.com/gosimple/slug"
)

var runes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
var runesAlpha = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var runesNumbers = []rune("1234567890")

func init() {
	rand.Seed(time.Now().UnixNano())
}

func CreateToken(total int) string {
	token := make([]rune, total)

	for i := range token {
		token[i] = runes[rand.Intn(len(runes))]
	}

	return string(token)
}

func CreateDiscriminator(username string) string {
	token := make([]rune, 4)

	for i := range token {
		token[i] = runesNumbers[rand.Intn(len(runesNumbers))]
	}

	username = username + string(token)

	return username
}

func CreateUsername(value string) string {
	username := slug.Make(value)

	return username + "-" + CreateToken(10)
}

func Fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
