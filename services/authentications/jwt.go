package authentications

import (
	"crypto/rsa"
	"io/ioutil"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/models"
)

var (
	signKey   *rsa.PrivateKey
	verifyKey *rsa.PublicKey
)

func init() {
	signBytes, err := ioutil.ReadFile(configs.PathPrivateKey)
	fatal(err)

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	fatal(err)

	verifyBytes, err := ioutil.ReadFile(configs.PathPublicKey)
	fatal(err)

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	fatal(err)
}

func Sign(account models.Account) (tokenString string, err error) {

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"ID":                 account.ID,
		"Username":           account.Username,
		"Confirmed":          account.Confirmed,
		"PasswordSet":        account.PasswordSet,
		"AvatarID":           account.Profile.Avatar.MediaID,
		"AvatarStatus":       account.Profile.Avatar.Status,
		"ConnectionFacebook": account.Connections.Facebook.Connected,
		"ConnectionGoogle":   account.Connections.Google.Connected,
		"ConnectionLinkedIn": account.Connections.LinkedIn.Connected,
		"NotBefore":          time.Now(),
		"ExpiresOn":          time.Now().Add(time.Minute * 60 * 24 * 7),
	})

	tokenString, err = token.SignedString(signKey)

	return tokenString, err
}

func CheckToken(myToken string) (pass bool, id string) {

	pass = false
	id = ""

	token, err := jwt.Parse(myToken, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})

	if err == nil && token.Valid {
		pass = true

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			id = claims["ID"].(string)
		}
	}

	return
}

//
func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
