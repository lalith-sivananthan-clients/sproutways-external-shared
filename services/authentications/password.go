package authentications

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

const (
	saltLength  = 64
	encryptCost = 10 // On a scale of 3 - 31, how intense Bcrypt should be
)

func CreatePassword(password string) string {
	hash := hash(password)

	return hash
}

func Check(hash string, guess string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(guess))

	if err != nil {
		return false
	}

	return true
}

func hash(password string) string {
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), encryptCost)

	if err != nil {
		log.Fatal(err)
	}

	return string(hashed)
}
