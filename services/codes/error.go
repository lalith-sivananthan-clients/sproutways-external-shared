package codes

const (
	//
	ErrorUnknown = 0

	// Params
	ErrorParams        = 1.0
	ErrorParamsInvalid = 1.1
	ErrorParamsMissing = 1.2
	ErrorParamsBind    = 1.3
	ErrorParamsConform = 1.4

	ErrorFile         = 7.0
	ErrorFileInvalid  = 7.1
	ErrorFileMissing  = 7.2
	ErrorFileOpen     = 7.3
	ErrorFileSave     = 7.4
	ErrorFileCreate   = 7.5
	ErrroFileUpload   = 7.6
	ErrroFileDownload = 7.7
	ErrroFileDelete   = 7.8

	// JWT
	ErrorJWT        = 2.0
	ErrorJWTNotSet  = 2.1
	ErrorJWTCreate  = 2.2
	ErrorJWTInvalid = 2.3

	ErrorToken        = 3.0
	ErrorTokenNotSet  = 3.1
	ErrorTokenInvalid = 3.2

	ErrorAccount                 = 4.0
	ErrorAccountInvalidPassword  = 4.1
	ErrorAccountAlreadyConfirmed = 4.2

	ErrorOAuth2           = 5.0
	ErrorOAuth2Token      = 5.1
	ErrorOAuth2User       = 5.2
	ErrorOAuth2IDCheck    = 5.3
	ErrorOAuth2EmailCheck = 5.4

	ErrorACL        = 6.0
	ErrorACLMissing = 6.1
	ErrorACLDenied  = 6.2

	ErrorDB           = 100.0
	ErrorDBConnection = 100.1

	ErrorModelAccount                   = 101.0
	ErrorModelAccountGet                = 101.1
	ErrorModelAccountCreate             = 101.2
	ErrorModelAccountUpdate             = 101.3
	ErrorModelAccountDelete             = 101.4
	ErrorModelAccountValidation         = 101.5
	ErrorModelAccountNotSet             = 101.6

	ErrorModelAccountEmail              = 102.0
	ErrorModelAccountEmailGet           = 102.1
	ErrorModelAccountEmailCreate        = 102.2
	ErrorModelAccountEmailUpdate        = 102.3
	ErrorModelAccountEmailDelete        = 102.4
	ErrorModelAccountEmailValidation    = 102.5
	ErrorModelAccountEmailNotSet        = 102.6
	ErrorModelAccountPassword           = 103.0
	ErrorModelAccountPasswordGet        = 103.1
	ErrorModelAccountPasswordCreate     = 103.2
	ErrorModelAccountPasswordUpdate     = 103.3
	ErrorModelAccountPasswordDelete     = 103.4
	ErrorModelAccountPasswordValidation = 103.5
	ErrorModelAccountPasswordNotSet     = 103.6
	ErrorModelAccountGroup              = 104.0
	ErrorModelAccountGroupGet           = 104.1
	ErrorModelAccountGroupCreate        = 104.2
	ErrorModelAccountGroupUpdate        = 104.3
	ErrorModelAccountGroupDelete        = 104.4
	ErrorModelAccountGroupValidation    = 104.5
	ErrorModelAccountGroupNotSet        = 104.6
	ErrorModelShop                      = 105.0
	ErrorModelShopGet                   = 105.1
	ErrorModelShopCreate                = 105.2
	ErrorModelShopUpdate                = 105.3
	ErrorModelShopDelete                = 105.4
	ErrorModelShopValidation            = 105.5
	ErrorModelShopNotSet                = 105.6
	ErrorModelShopCategory              = 106.0
	ErrorModelShopCategoryGet           = 106.1
	ErrorModelShopCategoryCreate        = 106.2
	ErrorModelShopCategoryUpdate        = 106.3
	ErrorModelShopCategoryDelete        = 106.4
	ErrorModelShopCategoryValidation    = 106.5
	ErrorModelShopCategoryNotSet        = 106.6
	ErrorModelShopProduct               = 107.0
	ErrorModelShopProductGet            = 107.1
	ErrorModelShopProductCreate         = 107.2
	ErrorModelShopProductUpdate         = 107.3
	ErrorModelShopProductDelete         = 107.4
	ErrorModelShopProductValidation     = 107.5
	ErrorModelShopProductNotSet         = 107.6

	ErrorModelCart            = 108.0
	ErrorModelCartGet         = 108.1
	ErrorModelCartCreate      = 108.2
	ErrorModelCartUpdate      = 108.3
	ErrorModelCartDelete      = 108.4
	ErrorModelCartValidation  = 108.5
	ErrorModelCartNotSet      = 108.6
	ErrorModelOrder           = 109.0
	ErrorModelOrderGet        = 109.1
	ErrorModelOrderCreate     = 109.2
	ErrorModelOrderUpdate     = 109.3
	ErrorModelOrderDelete     = 109.4
	ErrorModelOrderValidation = 109.5
	ErrorModelOrderNotSet     = 109.6

	ErrorModelStorageMedia           = 150.0
	ErrorModelStorageMediaGet        = 150.1
	ErrorModelStorageMediaCreate     = 150.2
	ErrorModelStorageMediaUpdate     = 150.3
	ErrorModelStorageMediaDelete     = 150.4
	ErrorModelStorageMediaValidation = 150.5
	ErrorModelStorageMediaNotSet     = 150.6
)
