package validations

import (
	"regexp"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"gopkg.in/go-playground/validator.v9"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
)

var Validate *validator.Validate

func init() {
	Validate = validator.New()

	Validate.RegisterValidation("mongodID", validateMongoID)
	Validate.RegisterValidation("username", validateUsername)
	Validate.RegisterValidation("usernameUnique", validateUsernameUnique)
	Validate.RegisterValidation("emailUnique", validateEmailUnique)
	Validate.RegisterValidation("tokenPassword", validateTokenPassword)
	Validate.RegisterValidation("tokenEmail", validateTokenEmail)
}

func validateMongoID(fl validator.FieldLevel) bool {

	if bson.IsObjectIdHex(fl.Field().String()) {
		return true
	}

	return false
}

func validateUsername(fl validator.FieldLevel) bool {

	reg := regexp.MustCompile("^[a-zA-Z0-9-_]*$")

	if reg.MatchString(fl.Field().String()) {
		return true
	}

	return false
}

func validateUsernameUnique(fl validator.FieldLevel) bool {

	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> users : query
	query := bson.M{
		"un": fl.Field().String(),
	}

	// db : account -> users : count
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if err == nil && count == 0 {
		return true
	}

	return false
}

func validateEmailUnique(fl validator.FieldLevel) bool {

	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> users : query
	query := bson.M{
		"em": fl.Field().String(),
	}

	// db : account -> users : count
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if err == nil && count == 0 {
		return true
	}

	return false
}

func validateTokenPassword(fl validator.FieldLevel) bool {

	field := strings.Split(fl.Field().String(), "-0-")

	if !bson.IsObjectIdHex(field[0]) || !bson.IsObjectIdHex(field[1]) {
		return false
	}

	accountID := bson.ObjectIdHex(field[0])
	accountPasswordID := bson.ObjectIdHex(field[1])
	token := field[2]

	// db : refresh
	models.DBAuthenticationSessionAccountPasswords.Refresh()

	// db : account -> password : get
	accountPassword := &models.AccountPassword{}
	err := models.DBAuthenticationCollectionAccountPasswords.FindId(accountPasswordID).One(&accountPassword)

	if err != nil {
		return false
	}

	count, err := models.DBAuthenticationCollectionAccountPasswords.FindId(accountPasswordID).Count()

	if err != nil || count == 0 {
		return false
	}

	// TODO
	// check expiration date

	if accountPassword.Token == token && accountPassword.AccountID == accountID {
		return true
	}

	return false
}

func validateTokenEmail(fl validator.FieldLevel) bool {

	field := strings.Split(fl.Field().String(), "-0-")

	if !bson.IsObjectIdHex(field[0]) || !bson.IsObjectIdHex(field[1]) {
		return false
	}

	accountID := bson.ObjectIdHex(field[0])
	accountEmailID := bson.ObjectIdHex(field[1])
	token := field[2]

	// db : refresh
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// db : account -> email : get
	accountEmail := &models.AccountEmail{}
	err := models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).One(&accountEmail)

	spew.Dump("db : account -> email : get")
	spew.Dump(field[1], accountEmailID, err, accountEmail)

	if err != nil {
		return false
	}

	count, err := models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).Count()

	spew.Dump("db : account -> email : count")
	spew.Dump(count, err)

	if err != nil || count == 0 {
		return false
	}

	// TODO
	// check expiration date

	if accountEmail.Token == token && accountEmail.AccountID == accountID {
		return true
	}

	return false
}
