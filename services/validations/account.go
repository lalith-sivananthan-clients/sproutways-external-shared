package validations

import (
	"errors"

	"github.com/davecgh/go-spew/spew"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
)

func EmailCheck(email string) (err error) {

	if len(email) == 0 {
		err = errors.New("email is missing")
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"em": email,
	}

	// db : account -> get
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if count > 0 && err == nil {
		err = errors.New("email already exist")
		return
	}

	return
}

func AccountEmailCheck(email string, account *models.Account) (err error) {

	if len(email) == 0 {
		err = errors.New("email is missing")
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"$and": []interface{}{
			bson.M{
				"em": bson.M{
					"$eq": email,
				},
			},
			bson.M{
				"_id": bson.M{
					"$ne": account.ID,
				},
			},
		},
	}

	// db : account -> get
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if count > 0 && err == nil {
		err = errors.New("email already exist")
		return
	}

	return
}

func FacebookUserIDCheck(userID string) (exist bool, err error) {

	exist = false

	if len(userID) == 0 {
		err = errors.New("ID is missing")
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"cn.fb.id": userID,
	}

	// db : account -> get
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if count > 0 && err == nil {
		exist = true
		return
	}

	return
}

func GoogleUserIDCheck(userID string) (exist bool, err error) {

	exist = false

	if len(userID) == 0 {
		err = errors.New("ID is missing")
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"cn.go.id": userID,
	}

	// db : account -> get
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if count > 0 && err == nil {
		exist = true
		return
	}

	return
}

func LinkedInUserIDCheck(userID string) (exist bool, err error) {

	exist = false

	if len(userID) == 0 {
		spew.Dump(1)
		err = errors.New("ID is missing")
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"cn.li.id": userID,
	}

	// db : account -> get
	count, err := models.DBAuthenticationCollectionAccounts.Find(query).Count()

	if count > 0 && err == nil {
		exist = true
		return
	}

	return
}
