package middlewares

import (
	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/controllers"
	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
	"git.sipher.co/sproutways/external/shared/services/codes"
)

func AuthenticationCheck(c *gin.Context) {

	var token string

	if value, ok := c.Request.Header["Authorization"]; ok {
		token = value[0]
	} else {
		c.JSON(403, controllers.Response(true, codes.ResultError, codes.ErrorJWTNotSet))
		c.Abort()
		return
	}

	pass, id := authentications.CheckToken(string(token))
	accountID := bson.ObjectIdHex(id)

	if !pass || len(id) == 0 {
		c.JSON(403, controllers.Response(true, codes.ResultError, codes.ErrorJWTInvalid))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> get
	var account models.Account
	err := models.DBAuthenticationCollectionAccounts.FindId(accountID).One(&account)

	spew.Dump(err)

	if err != nil {
		c.JSON(403, controllers.Response(true, codes.ResultError, codes.ErrorModelAccountGet))
		c.Abort()
		return
	}

	c.Set("account", account)
	c.Next()
}
