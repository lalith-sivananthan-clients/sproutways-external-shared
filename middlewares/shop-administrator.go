package middlewares

import (
	"github.com/gin-gonic/gin"
	"github.com/leebenson/conform"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/controllers"
	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/codes"
	"git.sipher.co/sproutways/external/shared/services/validations"
)

func ShopAdministrator(c *gin.Context) {
	// parameters -> bind
	var params controllers.RequestShopSingleStruct
	c.Bind(&params)

	err := c.Bind(&params)

	if err != nil {
		c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)

	if err != nil {
		c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)

	if err != nil {
		c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	shopID := bson.ObjectIdHex(params.ShopID)

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBDataSessionShops.Refresh()

	// db : shop -> get
	shop := models.Shop{}
	err = models.DBDataCollectionShops.FindId(shopID).One(&shop)

	if err != nil {
		c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorModelShopGet))
		c.Abort()
		return
	}

	if shop.AccountID == account.ID {
		c.Set("shop", &shop)
		c.Next()
		return
	}

	c.JSON(200, controllers.Response(true, codes.ResultError, codes.ErrorParamsInvalid))
	c.Abort()
	return
}
