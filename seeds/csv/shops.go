package csv

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
)

func CSVShops(path string) {
	spew.Dump(path)
	csvFile, err := os.Open(path)
	spew.Dump(err)
	reader := csv.NewReader(bufio.NewReader(csvFile))

	count := 0

	for {
		line, error := reader.Read()

		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		if count != 0 {
			// process image

			// process account
			account := &models.Account{
				ID:          bson.NewObjectId(),
				Email:       line[0],
				Username:    line[1],
				Password:    authentications.CreatePassword(line[2]),
				PasswordSet: true,
				Confirmed:   true,
				Status:      "active",
				Connections: models.AccountConnections{
					Facebook: models.AccountConnectionsFacebook{
						Connected: false,
					},
					Google: models.AccountConnectionsGoogle{
						Connected: false,
					},
					LinkedIn: models.AccountConnectionsLinkedIn{
						Connected: false,
					},
				},
				Profile: models.AccountProfile{
					NameFirst: line[5],
					NameLast:  line[6],
					Headline:  line[7],
					Story:     line[8],
				},
				When: models.WhenNew(),
			}

			spew.Dump(account)
		}

		count++
	}
}
