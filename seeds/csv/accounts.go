package csv

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
	"git.sipher.co/sproutways/external/shared/services/media"
)

func CSVAccount(path string) {

	spew.Dump(path)
	csvFile, err := os.Open(path)
	spew.Dump(err)
	reader := csv.NewReader(bufio.NewReader(csvFile))

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBStorageSessionMedia.Refresh()

	count := 0

	for {
		line, error := reader.Read()

		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		if count != 0 {
			account := &models.Account{}
			query := bson.M{
				"em": line[0],
			}

			// db : account -> get
			err := models.DBAuthenticationCollectionAccounts.Find(query).One(&account)

			if err != nil {
				// db : account -> data
				account = &models.Account{
					ID:          bson.NewObjectId(),
					Email:       line[0],
					Username:    line[1],
					Password:    authentications.CreatePassword(line[2]),
					PasswordSet: true,
					Confirmed:   true,
					Status:      "active",
					Connections: models.AccountConnections{
						Facebook: models.AccountConnectionsFacebook{
							Connected: false,
						},
						Google: models.AccountConnectionsGoogle{
							Connected: false,
						},
						LinkedIn: models.AccountConnectionsLinkedIn{
							Connected: false,
						},
					},
					Profile: models.AccountProfile{
						Avatar: models.MediaImageRef{
							MediaID: "",
							Status:  "notset",
						},
						NameFirst: line[5],
						NameLast:  line[6],
						Headline:  line[7],
						Story:     line[8],
					},
					When: models.WhenNew(),
				}

				// db : account -> create
				err = models.DBAuthenticationCollectionAccounts.Insert(account)
				if err != nil {
					spew.Dump(err)
				}

				// db : media -> data
				image := &models.Media{
					ID:        bson.NewObjectId(),
					AccountID: account.ID,
					Type:      "image",
					Location:  "storage/images/",
					When:      models.WhenNew(),
				}

				err := media.CopyPaste(line[3], line[4], configs.PathGatewayMedia+image.Location, image.ID.Hex())
				if err != nil {
					spew.Dump(err)
				}

				err = media.Crop(configs.PathGatewayMedia+image.Location, image.ID.Hex(), 0, 0, 500, 500)
				if err != nil {
					spew.Dump(err)
				}

				err = media.Crop(configs.PathGatewayMedia+image.Location, image.ID.Hex(), 0, 0, 250, 250)
				if err != nil {
					spew.Dump(err)
				}

				err = media.Crop(configs.PathGatewayMedia+image.Location, image.ID.Hex(), 0, 0, 100, 100)
				if err != nil {
					spew.Dump(err)
				}

				// db : media -> create
				err = models.DBStorageCollectionMedia.Insert(image)
				if err != nil {
					spew.Dump(err)
				}

				// update acccount
			}
		}

		count++
	}
}
