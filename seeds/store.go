package seeds

import (
	"log"

	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
)

var shopStartRunes = []string{
	"Sight",
	"Legend",
	"Riddle",
	"Push",
	"Trail",
	"Turbo",
	"Holo",
	"Fusion",
	"Discover",
	"Connect",
}

var shopEndRunes = []string{
	"Point",
	"Way",
	"Circle",
	"Lounge",
	"Market",
	"Box",
	"Stable",
	"Realm",
	"System",
	"Fit",
}

var productStartRunes = []string{
	"Red",
	"Violet",
	"Green",
	"Blue",
	"Orange",
	"Pink",
	"Gray",
	"Yellow",
	"Black",
	"White",
}

var productEndRunes = []string{
	"Jillybean",
	"Trainwreck",
	"Mist",
	"Tangle",
	"Amnesia",
	"Ghost",
	"Kush",
	"Lavender",
	"Lights",
	"Bubba",
}

var categoryStartRunes = []string{
	"Ruby",
	"Pearl",
	"Topaz",
	"Jade",
	"Amethyst",
	"Sapphire",
	"Amber",
	"Emerald",
	"Diamond",
	"Moonstone",
}

//Shop struct {
//	ID        bson.ObjectId `bson:"_id,omitempty"  json:"ID"`
//	AccountID bson.ObjectId `bson:"_aid,omitempty" json:"AccountID"`
//	Name      string        `bson:"na"             json:"Name"`
//	Slug      string        `bson:"sl"             json:"Slug"`
//	Story     string        `bson:"st"             json:"Story"`
//	Location  ShopLocation  `bson:"lo"             json:"Location"`
//	Phone     ShopPhone     `bson:"ph"             json:"Phone"`
//	Settings  ShopSettings  `bson:"se"             json:"Settings"`
//	When      When          `bson:"wh"             json:"When"`
//}

func Shops() {
	log.Print("Start Seeding : Shops\n")

	documents := []models.Account{
		{
			ID:          bson.NewObjectId(),
			Username:    "admin",
			Email:       "admin@sipher.co",
			Password:    authentications.CreatePassword("password"),
			PasswordSet: true,
			Confirmed:   true,
			Status:      "active",
			Connections: models.AccountConnections{
				Facebook: models.AccountConnectionsFacebook{
					Connected: false,
				},
				Google: models.AccountConnectionsGoogle{
					Connected: false,
				},
				LinkedIn: models.AccountConnectionsLinkedIn{
					Connected: false,
				},
			},
			When: models.WhenNew(),
		},
	}

	for _, document := range documents {
		user := models.Account{}
		query := bson.M{
			"em": document.Email,
		}

		err := models.DBAuthenticationCollectionAccounts.Find(query).One(&user)

		if err != nil {
			log.Print("New\n")
			err = models.DBAuthenticationCollectionAccounts.Insert(document)

			if err != nil {
				log.Print(err)
			}
		}
	}
}
