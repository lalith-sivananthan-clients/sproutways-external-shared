package seeds

import (
	"log"

	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
)

func Accounts() {
	log.Print("Start Seeding : Accounts\n")

	documents := []models.Account{
		{
			ID:          bson.NewObjectId(),
			Username:    "admin",
			Email:       "admin@sipher.co",
			Password:    authentications.CreatePassword("password"),
			PasswordSet: true,
			Confirmed:   true,
			Status:      "active",
			Connections: models.AccountConnections{
				Facebook: models.AccountConnectionsFacebook{
					Connected: false,
				},
				Google: models.AccountConnectionsGoogle{
					Connected: false,
				},
				LinkedIn: models.AccountConnectionsLinkedIn{
					Connected: false,
				},
			},
			Profile: models.AccountProfile{
				Avatar: models.MediaImageRef{
					MediaID: "",
					Status:  "notset",
				},
				NameFirst: "Ad",
				NameLast:  "Min",
				Headline:  "",
				Story:     "",
			},
			When: models.WhenNew(),
		},
		{
			ID:          bson.NewObjectId(),
			Username:    "moderator",
			Email:       "moderator@sipher.co",
			Password:    authentications.CreatePassword("password"),
			PasswordSet: true,
			Confirmed:   true,
			Status:      "active",
			Connections: models.AccountConnections{
				Facebook: models.AccountConnectionsFacebook{
					Connected: false,
				},
				Google: models.AccountConnectionsGoogle{
					Connected: false,
				},
				LinkedIn: models.AccountConnectionsLinkedIn{
					Connected: false,
				},
			},
			Profile: models.AccountProfile{
				Avatar: models.MediaImageRef{
					MediaID: "",
					Status:  "notset",
				},
				NameFirst: "Mod",
				NameLast:  "Erator",
				Headline:  "",
				Story:     "",
			},
			When: models.WhenNew(),
		},
		{
			ID:          bson.NewObjectId(),
			Username:    "editor",
			Email:       "editor@sipher.co",
			Password:    authentications.CreatePassword("password"),
			PasswordSet: true,
			Confirmed:   true,
			Status:      "active",
			Connections: models.AccountConnections{
				Facebook: models.AccountConnectionsFacebook{
					Connected: false,
				},
				Google: models.AccountConnectionsGoogle{
					Connected: false,
				},
				LinkedIn: models.AccountConnectionsLinkedIn{
					Connected: false,
				},
			},
			Profile: models.AccountProfile{
				Avatar: models.MediaImageRef{
					MediaID: "",
					Status:  "notset",
				},
				NameFirst: "Ed",
				NameLast:  "Itor",
				Headline:  "",
				Story:     "",
			},
			When: models.WhenNew(),
		},
		{
			ID:          bson.NewObjectId(),
			Username:    "reader",
			Email:       "reader@sipher.co",
			Password:    authentications.CreatePassword("password"),
			PasswordSet: true,
			Confirmed:   true,
			Status:      "active",
			Connections: models.AccountConnections{
				Facebook: models.AccountConnectionsFacebook{
					Connected: false,
				},
				Google: models.AccountConnectionsGoogle{
					Connected: false,
				},
				LinkedIn: models.AccountConnectionsLinkedIn{
					Connected: false,
				},
			},
			Profile: models.AccountProfile{
				Avatar: models.MediaImageRef{
					MediaID: "",
					Status:  "notset",
				},
				NameFirst: "Read",
				NameLast:  "Er",
				Headline:  "",
				Story:     "",
			},
			When: models.WhenNew(),
		},
	}

	for _, document := range documents {
		user := models.Account{}
		query := bson.M{
			"em": document.Email,
		}

		err := models.DBAuthenticationCollectionAccounts.Find(query).One(&user)

		if err != nil {
			log.Print("New\n")
			err = models.DBAuthenticationCollectionAccounts.Insert(document)

			if err != nil {
				log.Print(err)
			}
		}
	}
}
