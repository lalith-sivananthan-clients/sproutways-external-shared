package controllers

type (
	RequestArchiveStruct struct {
		Page int `form:"Page" conform:"trim"`
	}

	RequestSingleStruct struct {
		ID string `form:"ID" conform:"trim" binding:"required" validate:"required,mongodID"`
	}

	RequestShopSingleStruct struct {
		ShopID string `form:"ShopID" conform:"trim" binding:"required" validate:"required,mongodID"`
	}

	//

	ResponseStruct struct {
		Success bool         `json:"Success"`
		Result  ResultStruct `json:"Result"`
	}

	ResponseArchiveStruct struct {
		Success bool                `json:"Success"`
		Result  ResultArchiveStruct `json:"Result"`
	}

	ResultStruct struct {
		Code int         `json:"Code"`
		Data interface{} `json:"Data"`
	}

	ResultArchiveStruct struct {
		Code        int         `json:"Code"`
		PageCurrent int         `form:"PageCurrent"`
		PageTotal   int         `form:"PageTotal"`
		Data        interface{} `form:"Data"`
	}
)

func Response(success bool, code int, data interface{}) (response ResponseStruct) {
	response.Success = success
	response.Result.Code = code
	response.Result.Data = data

	return
}

func ResponseArchive(success bool, code int, page int, total int, data interface{}) (response ResponseArchiveStruct) {
	response.Success = success
	response.Result.Code = code
	response.Result.PageCurrent = page
	response.Result.PageTotal = total
	response.Result.Data = data

	return
}
