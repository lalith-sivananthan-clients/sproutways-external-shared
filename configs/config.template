package configs

import "os"

type (
	WebsiteStruct struct {
		Version   string
		URI       string
		URIOrigin string
	}

	WebsitesStruct struct {
		Production  WebsiteStruct
		Staging     WebsiteStruct
		Development WebsiteStruct
	}

	ServerStruct struct {
		Hostname string
		IP       string
		Port     string
	}

	ServersStruct struct {
		DBPostgres        ServerStruct
		DBMongo           ServerStruct
		DaemonQueue       ServerStruct
		WebHome           ServerStruct
		WebPlatform       ServerStruct
		GatewayConnect    ServerStruct
		GatewayMedia      ServerStruct
		APIAuthentication ServerStruct
		APIConnect        ServerStruct
		APIAccount        ServerStruct
		APIMedia          ServerStruct
		APIShop           ServerStruct
		APISearch         ServerStruct
		APIInbox          ServerStruct
		APISocialize      ServerStruct
	}

	HostnameStruct struct {
		Production  []string
		Staging     []string
		Development []string
	}
)

const (
	VersionProduction  = "production"
	VersionStaging     = "staging"
	VersionDevelopment = "development"

	Application = "Sproutways"
	Domain      = "sproutways.com"
	Version     = VersionStaging

	//
	// Providers

	ProviderFacebook = "facebook"
	ProviderGoogle   = "google"
	ProviderLinkedIn = "linkedin"
)

var (
	//
	// Path

	Path             = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/shared/"
	PathPrivateKey   = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/shared/configs/keys/app.rsa"
	PathPublicKey    = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/shared/configs/keys/app.rsa.pub"
	PathDaemonQueue  = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/daemon/queue/"
	PathGatewayMedia = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/gateway/media/"

	//
	// Server Env

	ServersProduction = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "127.0.0.1", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "127.0.0.1", ":9042",
		},

		WebHome: ServerStruct{
			"web-home-01", "127.0.0.1", ":7000",
		},
		WebPlatform: ServerStruct{
			"web-platform-01", "127.0.0.1", ":7001",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "127.0.0.1", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "127.0.0.1", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "127.0.0.1", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "127.0.0.1", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "127.0.0.1", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "127.0.0.1", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "127.0.0.1", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "127.0.0.1", ":9005",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "127.0.0.1", ":9006",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "127.0.0.1", ":9007",
		},
	}

	ServersStaging = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "172.31.13.20", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "172.31.13.20", ":27017",
		},

		WebHome: ServerStruct{
			"web-home-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":7000",
		},
		WebPlatform: ServerStruct{
			"web-platform-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":7001",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9005",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9006",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "ec2-54-241-143-203.us-west-1.compute.amazonaws.com", ":9007",
		},
	}

	ServersDevelopment = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "127.0.0.1", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "127.0.0.1", ":27017",
		},

		WebHome: ServerStruct{
			"web-home-01", "127.0.0.1", ":7000",
		},
		WebPlatform: ServerStruct{
			"web-platform-01", "127.0.0.1", ":7001",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "127.0.0.1", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "127.0.0.1", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "127.0.0.1", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "127.0.0.1", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "127.0.0.1", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "127.0.0.1", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "127.0.0.1", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "127.0.0.1", ":9005",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "127.0.0.1", ":9006",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "127.0.0.1", ":9007",
		},
	}

	//
	// Website Env

	WebsitesHome = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "http://" + Domain + "/",
			URIOrigin: "http://" + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebHome.IP + ServersStaging.WebHome.Port + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebHome.IP + ServersDevelopment.WebHome.Port + "/",
			URIOrigin: "*",
		},
	}

	WebsitesPlatform = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "http://" + Domain + "/",
			URIOrigin: "http://" + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "http://" + ServersStaging.WebPlatform.IP + ServersStaging.WebPlatform.Port + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebPlatform.IP + ServersDevelopment.WebPlatform.Port + "/",
			URIOrigin: "*",
		},
	}
)

func GetHostname() (result string) {

	result, err := os.Hostname()

	if err != nil {
		panic(err)
	}

	return
}

func GetServer() (result ServersStruct) {

	switch Version {
	case VersionProduction:
		result = ServersProduction
	case VersionStaging:
		result = ServersStaging
	case VersionDevelopment:
		result = ServersDevelopment
	}

	return
}

func GetWebsiteHomeURI() (result string) {

	switch Version {
	case VersionProduction:
		result = WebsitesHome.Production.URI
	case VersionStaging:
		result = WebsitesHome.Staging.URI
	case VersionDevelopment:
		result = WebsitesHome.Development.URI
	}

	return
}

func GetWebsiteHomeURIOrigin() (result string) {

	switch Version {
	case VersionProduction:
		result = WebsitesHome.Production.URIOrigin
	case VersionStaging:
		result = WebsitesHome.Staging.URIOrigin
	case VersionDevelopment:
		result = WebsitesHome.Development.URIOrigin
	}

	return
}

func GetWebsitePlatformURI() (result string) {

	switch Version {
	case VersionProduction:
		result = WebsitesPlatform.Production.URI
	case VersionStaging:
		result = WebsitesPlatform.Staging.URI
	case VersionDevelopment:
		result = WebsitesPlatform.Development.URI
	}

	return
}

func GetWebsitePlatformURIOrigin() (result string) {

	switch Version {
	case VersionProduction:
		result = WebsitesPlatform.Production.URIOrigin
	case VersionStaging:
		result = WebsitesPlatform.Staging.URIOrigin
	case VersionDevelopment:
		result = WebsitesPlatform.Development.URIOrigin
	}

	return
}
