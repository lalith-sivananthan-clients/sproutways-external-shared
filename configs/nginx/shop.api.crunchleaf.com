server {
	server_name www.shop.api.crunchleaf.com shop.api.crunchleaf.com;

	return 301 https://shop.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name www.shop.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/shop.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/shop.api.crunchleaf.com/privkey.pem;

	return 301 https://shop.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name shop.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/shop.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/shop.api.crunchleaf.com/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:9004;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $host;
		proxy_cache_bypass $http_upgrade;
	}
}