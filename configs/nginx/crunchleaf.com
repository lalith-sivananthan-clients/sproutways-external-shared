server {
    server_name www.crunchleaf.com crunchleaf.com;

    return 301 https://crunchleaf.com$request_uri;
}

server {
    listen 443 ssl;

    server_name www.crunchleaf.com;
    ssl_certificate /etc/letsencrypt/live/crunchleaf.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/crunchleaf.com/privkey.pem;

    return 301 https://crunchleaf.com$request_uri;
}

server {
    listen 443 ssl;

    server_name crunchleaf.com;
    ssl_certificate /etc/letsencrypt/live/crunchleaf.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/crunchleaf.com/privkey.pem;

    root /home/ubuntu/go/src/git.sipher.co/sproutways/external/web/platform/dist/platform;
    index index.html index.htm;

    location / {
        try_files $uri $uri/ /index.html;
    }
}