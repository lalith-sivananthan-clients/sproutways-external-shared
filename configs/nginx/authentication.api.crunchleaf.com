server {
	server_name www.authentication.api.crunchleaf.com authentication.api.crunchleaf.com;

	return 301 https://authentication.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name www.authentication.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/authentication.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/authentication.api.crunchleaf.com/privkey.pem;

	return 301 https://authentication.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name authentication.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/authentication.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/authentication.api.crunchleaf.com/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:9000;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $host;
		proxy_cache_bypass $http_upgrade;
	}
}