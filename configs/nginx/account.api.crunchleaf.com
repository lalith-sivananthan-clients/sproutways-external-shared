server {
	server_name www.account.api.crunchleaf.com account.api.crunchleaf.com;

	return 301 https://account.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name www.account.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/account.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/account.api.crunchleaf.com/privkey.pem;

	return 301 https://account.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name account.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/account.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/account.api.crunchleaf.com/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:9002;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $host;
		proxy_cache_bypass $http_upgrade;
	}
}