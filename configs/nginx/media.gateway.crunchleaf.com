server {
	server_name www.media.gateway.crunchleaf.com media.gateway.crunchleaf.com;

	return 301 https://media.gateway.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name www.media.gateway.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/media.gateway.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/media.gateway.crunchleaf.com/privkey.pem;

	return 301 https://media.gateway.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name media.gateway.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/media.gateway.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/media.gateway.crunchleaf.com/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:8001;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $host;
		proxy_cache_bypass $http_upgrade;
	}
}