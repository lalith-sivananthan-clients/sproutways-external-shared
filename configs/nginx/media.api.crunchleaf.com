server {
	server_name www.media.api.crunchleaf.com media.api.crunchleaf.com;

	return 301 https://media.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name www.media.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/media.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/media.api.crunchleaf.com/privkey.pem;

	return 301 https://media.api.crunchleaf.com$request_uri;
}

server {
	listen 443 ssl;

	server_name media.api.crunchleaf.com;
	ssl_certificate /etc/letsencrypt/live/media.api.crunchleaf.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/media.api.crunchleaf.com/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:9003;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $host;
		proxy_cache_bypass $http_upgrade;
	}
}