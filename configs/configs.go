package configs

import "os"

type (
	WebsiteStruct struct {
		Version   string
		URI       string
		URIOrigin string
	}

	WebsitesStruct struct {
		Production  WebsiteStruct
		Staging     WebsiteStruct
		Development WebsiteStruct
	}

	ServerStruct struct {
		Hostname string
		IP       string
		Port     string
	}

	ServersStruct struct {
		DBPostgres        ServerStruct
		DBMongo           ServerStruct
		DaemonQueue       ServerStruct
		WebHome           ServerStruct
		WebAccount        ServerStruct
		WebManage         ServerStruct
		WebMarketplace    ServerStruct
		WebBrokerage      ServerStruct
		GatewayConnect    ServerStruct
		GatewayMedia      ServerStruct
		APIAuthentication ServerStruct
		APIConnect        ServerStruct
		APIAccount        ServerStruct
		APIMedia          ServerStruct
		APIShop           ServerStruct
		APISearch         ServerStruct
		APITransaction    ServerStruct
		APIInbox          ServerStruct
		APISocialize      ServerStruct
	}

	HostnameStruct struct {
		Production  []string
		Staging     []string
		Development []string
	}
)

const (
	VersionProduction  = "production"
	VersionStaging     = "staging"
	VersionDevelopment = "development"

	Application = "Sproutways"
	Domain      = "sproutways.com"
	Version     = VersionProduction

	//
	// Providers

	ProviderFacebook = "facebook"
	ProviderGoogle   = "google"
	ProviderLinkedIn = "linkedin"
)

var (
	//
	// Path

	Path           = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/shared/"
	PathPrivateKey = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/shared/configs/keys/app.rsa"
	PathPublicKey  = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/shared/configs/keys/app.rsa.pub"

	PathAPIuthentication = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/authentication/"
	PathAPIConnect       = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/connect/"
	PathAPIAccount       = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/account/"
	PathAPIMedia         = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/media/"
	PathAPISearch        = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/search/"
	PathAPIShop          = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/shop/"
	PathAPIInbox         = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/inbox/"
	PathAPISocialize     = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/api/socialize/"

	PathDaemonInstall = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/daemon/install/"
	PathDaemonQueue   = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/daemon/queue/"

	PathGatewayConnect = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/gateway/connect/"
	PathGatewayMedia   = os.Getenv("GOPATH") + "/src/git.sipher.co/sproutways/external/gateway/media/"

	//
	// Server Env

	ServersProduction = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "127.0.0.1", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "172.16.1.11", ":27017",
		},

		WebHome: ServerStruct{
			"web-home-01", "127.0.0.1", ":7000",
		},
		WebAccount: ServerStruct{
			"web-account-01", "127.0.0.1", ":7001",
		},
		WebManage: ServerStruct{
			"web-manage-01", "127.0.0.1", ":7002",
		},
		WebMarketplace: ServerStruct{
			"web-marketplace-01", "127.0.0.1", ":7003",
		},
		WebBrokerage: ServerStruct{
			"web-brokerage-01", "127.0.0.1", ":7004",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "127.0.0.1", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "127.0.0.1", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "127.0.0.1", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "127.0.0.1", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "127.0.0.1", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "127.0.0.1", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "127.0.0.1", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "127.0.0.1", ":9005",
		},
		APITransaction: ServerStruct{
			"api-transaction-01", "127.0.0.1", ":9006",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "127.0.0.1", ":9007",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "127.0.0.1", ":9008",
		},
	}

	ServersStaging = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "172.31.13.20", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "172.31.13.20", ":27017",
		},

		WebHome: ServerStruct{
			"web-home-01", "127.0.0.1", ":7000",
		},
		WebAccount: ServerStruct{
			"web-account-01", "127.0.0.1", ":7001",
		},
		WebManage: ServerStruct{
			"web-manage-01", "127.0.0.1", ":7002",
		},
		WebMarketplace: ServerStruct{
			"web-marketplace-01", "127.0.0.1", ":7003",
		},
		WebBrokerage: ServerStruct{
			"web-brokerage-01", "127.0.0.1", ":7004",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "connect.gateway.crunchleaf.com", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "media.gateway.crunchleaf.com", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "127.0.0.1", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "127.0.0.1", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "127.0.0.1", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "127.0.0.1", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "127.0.0.1", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "127.0.0.1", ":9005",
		},
		APITransaction: ServerStruct{
			"api-transaction-01", "127.0.0.1", ":9006",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "127.0.0.1", ":9007",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "127.0.0.1", ":9008",
		},
	}

	ServersDevelopment = ServersStruct{
		DBPostgres: ServerStruct{
			"db-postgres-01", "127.0.0.1", ":3306",
		},
		DBMongo: ServerStruct{
			"db-mongo-01", "127.0.0.1", ":27017",
		},

		WebHome: ServerStruct{
			"web-home-01", "127.0.0.1", ":7000",
		},
		WebAccount: ServerStruct{
			"web-account-01", "127.0.0.1", ":7001",
		},
		WebManage: ServerStruct{
			"web-manage-01", "127.0.0.1", ":7002",
		},
		WebMarketplace: ServerStruct{
			"web-marketplace-01", "127.0.0.1", ":7003",
		},
		WebBrokerage: ServerStruct{
			"web-brokerage-01", "127.0.0.1", ":7004",
		},

		GatewayConnect: ServerStruct{
			"gateway-connect-01", "127.0.0.1", ":8000",
		},
		GatewayMedia: ServerStruct{
			"gateway-media-01", "127.0.0.1", ":8001",
		},

		APIAuthentication: ServerStruct{
			"api-authentication-01", "127.0.0.1", ":9000",
		},
		APIConnect: ServerStruct{
			"api-connect-01", "127.0.0.1", ":9001",
		},
		APIAccount: ServerStruct{
			"api-account-01", "127.0.0.1", ":9002",
		},
		APIMedia: ServerStruct{
			"api-media-01", "127.0.0.1", ":9003",
		},
		APIShop: ServerStruct{
			"api-shop-01", "127.0.0.1", ":9004",
		},
		APISearch: ServerStruct{
			"api-search-01", "127.0.0.1", ":9005",
		},
		APITransaction: ServerStruct{
			"api-transaction-01", "127.0.0.1", ":9006",
		},
		APIInbox: ServerStruct{
			"api-inbox-01", "127.0.0.1", ":9007",
		},
		APISocialize: ServerStruct{
			"api-socialize-01", "127.0.0.1", ":9008",
		},
	}

	//
	// Website Env

	WebHome = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "https://" + Domain + "/",
			URIOrigin: "https://" + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebHome.IP + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebHome.IP + ServersDevelopment.WebHome.Port + "/",
			URIOrigin: "*",
		},
	}

	WebAccount = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "https://account." + Domain + "/",
			URIOrigin: "https://account." + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebAccount.IP + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebAccount.IP + ServersDevelopment.WebAccount.Port + "/",
			URIOrigin: "*",
		},
	}

	WebManage = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "https://manage." + Domain + "/",
			URIOrigin: "https://manage." + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebManage.IP + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebManage.IP + ServersDevelopment.WebManage.Port + "/",
			URIOrigin: "*",
		},
	}

	WebMarketplace = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "https://marketplace." + Domain + "/",
			URIOrigin: "https://marketplace." + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebMarketplace.IP + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebMarketplace.IP + ServersDevelopment.WebMarketplace.Port + "/",
			URIOrigin: "*",
		},
	}

	WebBrokerage = WebsitesStruct{
		Production: WebsiteStruct{
			Version:   "production",
			URI:       "https://brokerage." + Domain + "/",
			URIOrigin: "https://brokerage." + Domain,
		},
		Staging: WebsiteStruct{
			Version:   "staging",
			URI:       "https://" + ServersStaging.WebBrokerage.IP + "/",
			URIOrigin: "*",
		},
		Development: WebsiteStruct{
			Version:   "development",
			URI:       "https://" + ServersDevelopment.WebBrokerage.IP + ServersDevelopment.WebBrokerage.Port + "/",
			URIOrigin: "*",
		},
	}
)

func GetHostname() (result string) {

	result, err := os.Hostname()

	if err != nil {
		panic(err)
	}

	return
}

func GetServer() (result ServersStruct) {

	switch Version {
	case VersionProduction:
		result = ServersProduction
	case VersionStaging:
		result = ServersStaging
	case VersionDevelopment:
		result = ServersDevelopment
	}

	return
}

func GetWebHome() (result WebsiteStruct) {

	switch Version {
	case VersionProduction:
		result = WebHome.Production
	case VersionStaging:
		result = WebHome.Staging
	case VersionDevelopment:
		result = WebHome.Development
	}

	return
}

func GetWebAccount() (result WebsiteStruct) {

	switch Version {
	case VersionProduction:
		result = WebAccount.Production
	case VersionStaging:
		result = WebAccount.Staging
	case VersionDevelopment:
		result = WebAccount.Development
	}

	return
}

func GetWebManage() (result WebsiteStruct) {

	switch Version {
	case VersionProduction:
		result = WebManage.Production
	case VersionStaging:
		result = WebManage.Staging
	case VersionDevelopment:
		result = WebManage.Development
	}

	return
}

func GetWebMarketplace() (result WebsiteStruct) {

	switch Version {
	case VersionProduction:
		result = WebMarketplace.Production
	case VersionStaging:
		result = WebMarketplace.Staging
	case VersionDevelopment:
		result = WebMarketplace.Development
	}

	return
}

func GetWebBrokerage() (result WebsiteStruct) {

	switch Version {
	case VersionProduction:
		result = WebBrokerage.Production
	case VersionStaging:
		result = WebBrokerage.Staging
	case VersionDevelopment:
		result = WebBrokerage.Development
	}

	return
}
