package configs

import "gopkg.in/mgo.v2"

const (
	DBName                                         = "sprout"
	DBAuthenticationCollectionAccountsName         = "auth_ac"
	DBAuthenticationCollectionAccountEmailsName    = "auth_ae"
	DBAuthenticationCollectionAccountPasswordsName = "auth_ap"
	DBStorageCollectionMediaName                   = "storage_me"
	DBQueueCollectionJobsName                      = "queue_jo"
	DBDataCollectionShopsName                      = "data_sh"
	DBDataCollectionShopProductsName               = "data_sp"
	DBDataCollectionTransactionCartsName           = "data_tc"
	DBDataCollectionTransactionOrdersName          = "data_to"

	DBSocializeCollectionFriendRequestName = "socialize_fr"
	DBSocializeCollectionFriendListName    = "socialize_fl"
	DBInboxCollectionConversation          = "inbox_cv"
	DBInboxCollectionMessage               = "inbox_ms"
)

func DBConnection(dbName string, collectionName string) (session *mgo.Session, collection *mgo.Collection) {

	var s *mgo.Session
	var err error

	switch Version {
	case VersionProduction:
		//s, err = mgo.DialWithInfo(&mgo.DialInfo{
		//	Addrs:    []string{GetServer().DBMongo.IP},
		//	Timeout:  60 * time.Second,
		//	Database: dbName,
		//	Username: "SproutAdmin",
		//	Password: "Sprout12321",
		//})

		s, err = mgo.Dial("mongodb://SproutAdmin:Sprout12321@" + GetServer().DBMongo.IP)
	case VersionStaging:
		s, err = mgo.Dial(GetServer().DBMongo.IP)
	case VersionDevelopment:
		s, err = mgo.Dial(GetServer().DBMongo.IP)
	}

	if err != nil {
		// TODO
		// cry to admin
	}

	s.SetMode(mgo.Monotonic, true)

	session = s.Copy()
	collection = s.DB(dbName).C(collectionName)

	return
}
