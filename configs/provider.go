package configs

type (
	ProviderStruct struct {
		ClientID      string
		Secret        string
		EndpointOAuth string
		EndpointAPI   string
		Callback      string
	}

	ProvidersStruct struct {
		Facebook ProviderStruct
		Google   ProviderStruct
		LinkedIn ProviderStruct
	}
)

var (
	ProviderProduction = &ProvidersStruct{
		Facebook: ProviderStruct{
			ClientID:      "2139273203067266",
			Secret:        "55f76b7252a4863fe32ef949b7d8a723",
			EndpointOAuth: "https://graph.facebook.com/v2.10/",
			EndpointAPI:   "https://graph.facebook.com/v2.10/",
			Callback:      GetWebAccount().URI + "connect/facebook/",
		},
		Google: ProviderStruct{
			ClientID:      "882674019824-om6roj9mhv8acmnkccjj0u2hs6pndcqo.apps.googleusercontent.com",
			Secret:        "Mf5XuS7PBGMnSYziBiEzkPOM",
			EndpointOAuth: "https://www.googleapis.com/oauth2/",
			EndpointAPI:   "https://www.googleapis.com/",
			Callback:      GetWebAccount().URI + "connect/google/",
		},
		LinkedIn: ProviderStruct{
			ClientID:      "78kpc73totne01",
			Secret:        "3XiHAtR9j3Rq2vhE",
			EndpointOAuth: "https://www.linkedin.com/oauth/v2/",
			EndpointAPI:   "https://www.linkedin.com/v1/",
			Callback:      GetWebAccount().URI + "connect/linkedin/",
		},
	}

	ProviderStaging = &ProvidersStruct{
		Facebook: ProviderStruct{
			ClientID:      "2139273203067266",
			Secret:        "55f76b7252a4863fe32ef949b7d8a723",
			EndpointOAuth: "https://graph.facebook.com/v2.10/",
			EndpointAPI:   "https://graph.facebook.com/v2.10/",
			Callback:      GetWebAccount().URI + "connect/facebook/",
		},
		Google: ProviderStruct{
			ClientID:      "882674019824-om6roj9mhv8acmnkccjj0u2hs6pndcqo.apps.googleusercontent.com",
			Secret:        "Mf5XuS7PBGMnSYziBiEzkPOM",
			EndpointOAuth: "https://www.googleapis.com/oauth2/",
			EndpointAPI:   "https://www.googleapis.com/",
			Callback:      GetWebAccount().URI + "connect/google/",
		},
		LinkedIn: ProviderStruct{
			ClientID:      "78kpc73totne01",
			Secret:        "3XiHAtR9j3Rq2vhE",
			EndpointOAuth: "https://www.linkedin.com/oauth/v2/",
			EndpointAPI:   "https://www.linkedin.com/v1/",
			Callback:      GetWebAccount().URI + "connect/linkedin/",
		},
	}

	ProviderDevelopment = &ProvidersStruct{
		Facebook: ProviderStruct{
			ClientID:      "2139273203067266",
			Secret:        "55f76b7252a4863fe32ef949b7d8a723",
			EndpointOAuth: "https://graph.facebook.com/v2.10/",
			EndpointAPI:   "https://graph.facebook.com/v2.10/",
			Callback:      GetWebAccount().URI + "connect/facebook/",
		},
		Google: ProviderStruct{
			ClientID:      "882674019824-om6roj9mhv8acmnkccjj0u2hs6pndcqo.apps.googleusercontent.com",
			Secret:        "Mf5XuS7PBGMnSYziBiEzkPOM",
			EndpointOAuth: "https://www.googleapis.com/oauth2/",
			EndpointAPI:   "https://www.googleapis.com/",
			Callback:      GetWebAccount().URI + "connect/google/",
		},
		LinkedIn: ProviderStruct{
			ClientID:      "78kpc73totne01",
			Secret:        "3XiHAtR9j3Rq2vhE",
			EndpointOAuth: "https://www.linkedin.com/oauth/v2/",
			EndpointAPI:   "https://www.linkedin.com/v1/",
			Callback:      GetWebAccount().URI + "connect/linkedin/",
		},
	}
)

func GetProvider() (result *ProvidersStruct) {

	switch Version {
	case VersionStaging:
		result = ProviderStaging
	case VersionProduction:
		result = ProviderProduction
	case VersionDevelopment:
		result = ProviderDevelopment
	}

	return
}
